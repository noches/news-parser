module.exports = {
    apps: [
        {
            name: "news-parser",
            script: "./server.js",
            exec_mode: process.env.PM2_EXEC_MODE,
            instances: process.env.PM2_CLUSTER_INSTANCES,
            instance_var: "SERVER_ID",
            ignore_watch: ["node_modules"],
            max_memory_restart: '300M',

            env: {
                NODE_ENV: process.env.NODE_ENV,
            }
        }
    ]
};
