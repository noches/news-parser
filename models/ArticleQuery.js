const fs = require('fs');
const articleDir = __dirname + '/../storage/articles/';
const files = new (require('./Files'))(articleDir);

class ArticleQuery {

    constructor(conditions = {}) {
        this.query = conditions.query;
        this.limit = conditions.limit;
        this.from = conditions.from;

        this.articles = this.readAndFilter();
    }

    getArticles() {
        return this.articles.sort(function (a, b) {
            if (a.created_at > b.created_at) {
                return -1;
            }
            if (b.created_at > a.created_at) {
                return 1;
            }

            return 0;
        });
    }

    readAndFilter() {
        let articleFiles = files.getFiles();
        let data = [];

        let i = 0;
        for (let key in articleFiles) {
            //apply limit filter
            if (typeof this.limit !== 'undefined' && i >= this.limit) {
                break;
            }

            const { content, timestamp } = files.readFile(articleFiles[key])

            //apply from filter
            if (typeof this.from !== 'undefined' && timestamp < this.from) {
                continue;
            }

            //search ignores lower and upper cases
            //apply query filter
            if (typeof this.query !== 'undefined'
                && content.toLowerCase().indexOf(this.query.toLowerCase()) < 0
            ) {
                continue;
            }

            i++;
            data.push({"content": content, "created_at": timestamp});
        }

        return data;
    }
}

module.exports = ArticleQuery;
