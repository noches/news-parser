const fs = require('fs');

class Files {

    constructor(dir) {
        this.dir = dir;
    }

    readFile(path) {
        let file = fs.openSync(this.dir + path, 'r'),
            content = fs.readFileSync(file, 'utf-8'),
            timestamp = fs.fstatSync(file).birthtimeMs.toFixed();

        return { content, timestamp }
    }

    //returns all files from directory
    getFiles() {
        let files = [];
        let readDir = fs.readdirSync(this.dir);

        for (let key in readDir) {
            //skip hidden files
            if (readDir[key][0] === '.') {
                continue;
            }

            files.push(readDir[key]);
        }

        return files;
    }
}

module.exports = Files;
