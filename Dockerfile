FROM node:14.15.4

WORKDIR /app

RUN yarn global add pm2
COPY ./package.json ./yarn.lock ./
RUN yarn

COPY . .

CMD ["pm2-runtime", "pm2.config.js"]
