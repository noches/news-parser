const NewsAPI = require('newsapi');
const newsapi = new NewsAPI(process.env.NEWS_API_KEY);
const crypto = require('crypto');
const fs = require('fs');

const articlesDir = __dirname + '/../storage/articles/';
const FIRST_PAGE = 1;
const PAGE_SIZE = 50;

const storeData = (data, filename) => {
    try {
        const filePath = articlesDir + filename;

        fs.stat(filePath, function(err, stats) {
            if (typeof stats !== 'undefined') {
                return;
            }

            fs.writeFile(filePath, data, function (err) {});
        });
    } catch (err) {
        console.error(err);
    }
}

const makeRequest = (page, pageSize) => {
    newsapi.v2.everything({
        sources: 'bbc-news',
        from: process.argv[2],
        to: process.argv[3],
        language: 'en',
        page: page,
        pageSize: pageSize
    }).then(response => {
        let status = response['status'];
        let totalResults = response['totalResults'];

        if (status !== "ok") {
            console.error('Request error');
            return 0;
        }

        for (let key in response['articles']) {
            if (!response['articles'].hasOwnProperty(key)) {
                return;
            }

            let article = response['articles'][key]
            let content = article['content'] === null ? '' : article['content'];
            //unique article id
            let hash = crypto.createHash('sha256').update(article['url'], 'utf-8').digest('hex');

            storeData(content, hash.slice(0, 20));
        }

        return totalResults;
    });
}

const getNews = (page) => {
    try {
        let totalResults = makeRequest(page, PAGE_SIZE);
        if (typeof totalResults === 'undefined') {
            return 0;
        }

        let leftArticles = totalResults - page * PAGE_SIZE;
        if (leftArticles <= 0) {
            return 0;
        }

        return leftArticles;
    } catch (e) {
        console.error(e.message);
    }

    return 0;
}

let page = FIRST_PAGE;
while (true) {
    let leftArticles = getNews(page);
    if (leftArticles === 0) {
        break;
    }

    page++;
}
