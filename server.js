const express = require("express");
const app = express();
const ArticleQuery = require('./models/ArticleQuery');

//TODO: add routes, controllers etc.
app.get('/news', (req, res) => {

    let limit = req.query.limit;
    let query = req.query.query;
    let from = req.query.from;

    res.json(
        new ArticleQuery({'limit': limit, 'query': query, 'from': from}).getArticles()
    );
});

// Respond with 404 to others.
app.use((req, res) => {
    res.status(404).json({'status': 'Page not found.'});
});

app.listen(process.env.HTTP_PORT);
