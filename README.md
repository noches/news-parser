### News parser:

At first, you need to set up your envs in .env file. You can use the example file:
```bash
cp .env.example .env
```

Build an image and run container:
```bash
docker build ./ -t news/parser:1.0
docker run -v $(pwd):/app/ --name news-parser -p 8056:80 --env-file ./.env news/parser:1.0
```

Script that's running article parsing:
```bash
docker exec -it news-parser node /app/scripts/parse-articles.js 2021-02-16 2021-02-17
```
Articles saved to `storage` directory.

Now you can use some api request like this to get articles:
```bash
curl --location --request GET '0.0.0.0:8056/news?limit=50&from=1612729951&query=Buendia'
```

For developing you need to add a watch flag in the Docker file.
```bash
CMD ["pm2-runtime", "pm2.config.js", "--watch"]
```
